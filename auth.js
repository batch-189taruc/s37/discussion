const jwt = require('jsonwebtoken');

const secret = "CourseBookingAPI";

module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	return jwt.sign(data, secret, {})//(payload, secretkey, {})
}
module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization;
	if(typeof token !== "undefined"){
		console.log(token);
		// shows : Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYyYzI4YzMwOWY1OTIwNzE0ZTk5ZGJhZSIsImVtYWlsIjoiZWZjaGlja2Vuc0BnbWFpbC5jb20iLCJpc0FkbWluIjpmYWxzZSwiaWF0IjoxNjU2OTg2NDg1fQ.AEtyUjcuNPqThAYjTd1dBIgz9EP4_ENGrl_-w-u7XKY
		token = token.slice(7, token.length);
		//shows: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYyYzI4YzMwOWY1OTIwNzE0ZTk5ZGJhZSIsImVtYWlsIjoiZWZjaGlja2Vuc0BnbWFpbC5jb20iLCJpc0FkbWluIjpmYWxzZSwiaWF0IjoxNjU2OTg2NDg1fQ.AEtyUjcuNPqThAYjTd1dBIgz9EP4_ENGrl_-w-u7XKY

		return jwt.verify(token, secret, (error, data) => {
			if(error) {
				return res.send({auth: "failed"})
			} else {
				next()
			}
		})
	} else {
		return res.send({auth: "failed"});
	}
}

module.exports.decode = (token) => {
	if(typeof token !== "undefined") {
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {
			if (err) {
				return null
			} else {
				return jwt.decode(token, {complete: true}).payload
			}
		})
	} else {
		return null
	}


}

