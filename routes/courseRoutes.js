const express = require('express');
const router = express.Router();

const courseController = require('../controllers/courseController');
const auth = require("../auth.js")



//Route for creating a course
router.post("/", (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	const isAdminData = userData.isAdmin
	if (isAdminData == true){
	courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController))}
	else{
		res.send("No access for creating a course")
	}
})

// route for retrieving all courses, with admin rights
router.get("/all", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	courseController.getAllCourses(userData).then(resultFromController => res.send(resultFromController))
})


router.get("/", (req,res) => {
	courseController.getAllActive().then(resultFromController => res.send(resultFromController))
});


//Route for retrieving a specific course

router.get("/:courseId", (req,res) => {
	console.log(req.params)
	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController))
})

//Route for updating a course

router.put("/:courseId", auth.verify, (req,res) => {
	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))
})


//ARCHIVING activity s40


router.put("/:courseId/archive", auth.verify, (req,res) => {
	courseController.archiveCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))
})


module.exports = router;



