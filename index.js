const express = require('express');
const mongoose = require('mongoose');
const dotenv = require('dotenv')
const port = 4000;
const cors = require('cors'); //allows our backend application to be available to our frontend application
const userRoutes = require('./routes/userRoutes.js')
const app = express();
const courseRoutes = require('./routes/courseRoutes.js')

dotenv.config()
app.use(cors())
app.use(express.json())
app.use(express.urlencoded({extended: true}));
app.use("/users", userRoutes);
app.use("/courses", courseRoutes);

mongoose.connect(`mongodb+srv://zuittdiscussion:${process.env.MONGODB_PASSWORD}@zuitt-bootcamp.xbtkvqt.mongodb.net/S37-S41?retryWrites=true&w=majority`, {
	useNewUrlParser : true,
	useUnifiedTopology : true
	}
)

let db = mongoose.connection;
db.on('error', () => console.error.bind(console, 'error'));
db.once('open', () => console.log('Now connected to MongoDB Atlas!'))


app.listen(process.env.PORT || port, () => {
	console.log(`API is now online on port ${process.env.PORT || port}`)
})
